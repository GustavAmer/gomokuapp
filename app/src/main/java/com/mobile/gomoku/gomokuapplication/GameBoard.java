package com.mobile.gomoku.gomokuapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Gustav on 2015-02-26.
 */
public class GameBoard extends View {


    private Point[][] mPoints = new Point[Game.GRID_SIZE][Game.GRID_SIZE];
    int mEdge = 0;
    float mSpace = 0;
    float mRadius = 0;
    Paint mBalckPaint;
    Paint mUserPaint;
    Paint mOpponentPaint;
    Paint mWinnerPaint;
    private boolean[][] mUserButtons;
    private boolean[][] mOpponentButtons;
    private Communication mCommunication;

    boolean gameOn = true;
    boolean mUserTurn = false;
    boolean localGame;

    private int[] winnerX = new int [5];
    private int[] winnerY = new int [5];

    public GameBoard(Context context){
        super(context);
        init();

    }

    public void setButtonArrays(boolean[][] user, boolean[][] opponent ){
        mUserButtons = user;
        mOpponentButtons = opponent;
    }

    private void init(){
        mBalckPaint = new Paint(Color.BLACK);
        mUserPaint = new Paint();
        mUserPaint.setColor(Color.RED);
        mOpponentPaint = new Paint();
        mOpponentPaint.setColor(Color.GREEN);

        mWinnerPaint = new Paint();
        mWinnerPaint.setColor(Color.BLUE);
        mWinnerPaint.setStyle(Paint.Style.STROKE);
        mWinnerPaint.setStrokeWidth(5);
        this.setOnTouchListener(mOnTouchListener);
    }

    public void setLocalGame(boolean b){
        localGame = b;
    }

    public void setCommunication(Communication c){
        mCommunication = c;
    }

    private long down;
    private OnTouchListener mOnTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = MotionEventCompat.getActionMasked(event);
            if(!gameOn)
                return true;
            if(action == MotionEvent.ACTION_DOWN)
                down = System.currentTimeMillis();
            if(action == MotionEvent.ACTION_UP) {
                if (System.currentTimeMillis() - down < 300) {
                    float x = event.getX();
                    float y = event.getY();
                    for (int i = 0; i < Game.GRID_SIZE; i++) {
                        for (int j = 0; j < Game.GRID_SIZE; j++) {
                            if (mPoints[i][j].inside(x, y, mRadius)) {
                                if (!mUserButtons[i][j] && !mOpponentButtons[i][j]) {
                                    if (mUserTurn) {
                                        mUserButtons[i][j] = true;
                                        if(winner(mUserButtons)) {
                                            gameOn = false;
                                            Toast.makeText(getContext(), "Winner!", Toast.LENGTH_LONG).show();
                                        }
                                        if(!localGame){
                                            mCommunication.transferMoves(mUserButtons, !gameOn, winnerX, winnerY);
                                        }

                                        mUserTurn = !mUserTurn;
                                    }
                                    else if(localGame){
                                        mOpponentButtons[i][j] = true;
                                        if(winner(mOpponentButtons)){
                                            gameOn = false;
                                            Toast.makeText(getContext(), "Winner!", Toast.LENGTH_LONG).show();
                                        }
                                        mUserTurn = !mUserTurn;
                                    }
                                    invalidate();
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
    };

    private void initPoints(){
        for (int i = 0; i < Game.GRID_SIZE; i++){
            for (int j = 0; j < Game.GRID_SIZE; j++){
                float x = i * mSpace;
                float y = j * mSpace;
                mPoints[i][j] = new Point(x, y);
            }
        }
    }



    @Override
    protected void onDraw(Canvas canvas){
        int height = getHeight();
        int width = getWidth();

        if(mEdge == 0 || mSpace == 0) {
            mEdge = height < width ? height : width;
            mEdge = mEdge - mEdge % (Game.GRID_SIZE - 1);
            mSpace = mEdge / (Game.GRID_SIZE - 1);
            mRadius = mSpace / 2;
            initPoints();
        }
        for (int i = 0; i < Game.GRID_SIZE; i++){
            canvas.drawLine(i * mSpace, 0, i * mSpace, mEdge, mBalckPaint);
            canvas.drawLine(0, i * mSpace, mEdge, i * mSpace, mBalckPaint);
        }
        for (int i = 0; i < Game.GRID_SIZE; i++) {
            for (int j = 0; j < Game.GRID_SIZE; j++) {
                if (mUserButtons[i][j])
                    canvas.drawCircle(mPoints[i][j].getX(), mPoints[i][j].getY(), mRadius, mUserPaint);
                else if (mOpponentButtons[i][j])
                    canvas.drawCircle(mPoints[i][j].getX(), mPoints[i][j].getY(), mRadius, mOpponentPaint);
            }
        }
        if(!gameOn){
            for (int i=0; i< winnerX.length; i++){
                canvas.drawCircle(mPoints[winnerX[i]][winnerY[i]].getX(), mPoints[winnerX[i]][winnerY[i]].getY(), mRadius + 2, mWinnerPaint);
            }
        }
    }

    public void resetGame(){
        gameOn = true;
        mUserTurn = false;
        invalidate();
    }


    public boolean winner(boolean[][] array){
        for (int i = 0; i < Game.GRID_SIZE; i++) {
            int diagonalCounter1 = 0;
            int[] diagArray1X = new int[5];
            int[] diagArray1Y = new int[5];

            int diagonalCounter2 = 0;
            int[] diagArray2X = new int[5];
            int[] diagArray2Y = new int[5];

            int diagonalCounter3 = 0;
            int[] diagArray3X = new int[5];
            int[] diagArray3Y = new int[5];

            int diagonalCounter4 = 0;
            int[] diagArray4X = new int[5];
            int[] diagArray4Y = new int[5];

            int verticalCounter = 0;
            int[] vertArrayX = new int[5];
            int[] vertArrayY = new int[5];

            int horizontalCounter = 0;
            int[] horArrayX = new int[5];
            int[] horArrayY = new int[5];
            for (int j = 0; j < Game.GRID_SIZE; j++) {
                if(j + i < Game.GRID_SIZE) {
                    if (array[j + i][j]) {//0,0; 1,1; 2,2;..;1,0;2,1;3,2  -- upper rigt
                        diagArray1X[diagonalCounter1] = j + i;
                        diagArray1Y[diagonalCounter1] = j;
                        diagonalCounter1++;
                        if (diagonalCounter1 == 5){
                            winnerX = diagArray1X;
                            winnerY = diagArray1Y;
                            return true;
                        }
                    } else
                        diagonalCounter1 = 0;


                    if (array[j][j + i]) { //0,1;1,2 -- lower right
                        diagArray2X[diagonalCounter2] = j;
                        diagArray2Y[diagonalCounter2] = j + i;
                        diagonalCounter2++;
                        if (diagonalCounter2 == 5){
                            winnerX = diagArray2X;
                            winnerY = diagArray2Y;
                            return true;
                        }
                    } else
                        diagonalCounter2 = 0;


                    if (array[Game.GRID_SIZE - 1 - (j + i)][j]) { //14,14;   upper left triangle

                        diagArray3X[diagonalCounter3] = Game.GRID_SIZE - 1 - (j + i);
                        diagArray3Y[diagonalCounter3] = j;
                        diagonalCounter3++;
                        if (diagonalCounter3 == 5){
                            winnerX = diagArray3X;
                            winnerY = diagArray3Y;
                            return true;
                        }
                    } else
                        diagonalCounter3 = 0;

                    if (array[Game.GRID_SIZE - 1 - (j)][j + i])  { //lower right trianlge
                        diagArray4X[diagonalCounter4] = Game.GRID_SIZE - 1 - j;
                        diagArray4Y[diagonalCounter4] = (j + i);
                        diagonalCounter4++;
                        if (diagonalCounter4 == 5){
                            winnerX = diagArray4X;
                            winnerY = diagArray4Y;
                            return true;
                        }
                    } else
                        diagonalCounter4 = 0;
                }
                if(array[i][j]) {
                    vertArrayX[verticalCounter] =  i;
                    vertArrayY[verticalCounter] = j;
                    verticalCounter++;
                    if(verticalCounter == 5){
                        winnerX = vertArrayX;
                        winnerY = vertArrayY;
                        return true;
                    }
                }else
                    verticalCounter = 0;
                if(array[j][i]) {
                    horArrayX[horizontalCounter] = j;
                    horArrayY[horizontalCounter] = i;

                    horizontalCounter++;
                    if(horizontalCounter == 5){
                        winnerX = horArrayX;
                        winnerY = horArrayY;
                        return true;
                    }
                }else
                    horizontalCounter = 0;
            }
        }
        return false;
    }

    public void opponentMoved(boolean[][] moves, boolean win, int[] winX, int[] winY){
        mOpponentButtons = moves;
        if(win){
            winnerX = winX;
            winnerY = winY;
            gameOn = false;
        }
        invalidate();
        mUserTurn = true;
    }



    public void setUserTurn(){
        mUserTurn = true;
    }
}
