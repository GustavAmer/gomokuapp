package com.mobile.gomoku.gomokuapplication;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Gustav on 2015-04-19.
 */
public class BTConnection extends Activity{

    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter myBluetoothAdapter;
    private Set<BluetoothDevice> pairedDevices;
    private OutputStream out;
    private InputStream in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_view);

        Log.e("bt", "start");


        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        pairedDevices = myBluetoothAdapter.getBondedDevices();

        Log.e("bt", pairedDevices.size()+ "");
        registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        if (!myBluetoothAdapter.isEnabled()) {
            Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);

            Toast.makeText(getApplicationContext(), "Bluetooth turned on",
                    Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"Bluetooth is already on",
                    Toast.LENGTH_LONG).show();
        }


        if(pairedDevices.size() > 0){
            Iterator <BluetoothDevice> it = pairedDevices.iterator();
            while(it.hasNext()){
                BluetoothDevice device = it.next();
                ParcelUuid[] uuids = device.getUuids();
                BluetoothSocket socket = null;
                try {
                    socket = device.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                    socket.connect();
                    out = socket.getOutputStream();
                    in = socket.getInputStream();
                    Log.e("bt", device.getName());
                    break;
                }catch(Exception e){
                    try {
                        socket.close();
                    }catch (Exception e1){}
                }
            }
            try {
                out.write("jeje".getBytes());
//                beginListenForData();
            }catch (Exception e){}
        }
    }

    static int readBufferPosition = 0;

    static boolean stopWorker = false;
    static byte[] readBuffer = new byte[1024];
    void beginListenForData()   {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        Thread workerThread = new Thread(new Runnable(){
            public void run(){
                while(!Thread.currentThread().isInterrupted() && !stopWorker){
                    try{
                        int bytesAvailable = in.available();
                        if(bytesAvailable > 0){
                            byte[] packetBytes = new byte[bytesAvailable];
                            in.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++){
                                byte b = packetBytes[i];
                                if(b == delimiter){
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable(){
                                        public void run(){
                                            Log.e("bt", data);
                                        }
                                    });
                                }
                                else{
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (IOException ex){
                        stopWorker = true;
                    }
                }
            }
        });

        workerThread.start();
    }
    final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // add the name and the MAC address of the object to the arrayAdapter
                pairedDevices.add(device);
                Log.e("bt","device found" + device.getAddress());
            }
        }
    };

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        unregisterReceiver(bReceiver);
    }

}
