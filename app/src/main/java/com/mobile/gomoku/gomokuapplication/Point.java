package com.mobile.gomoku.gomokuapplication;

/**
 * Created by Gustav on 2015-03-03.
 */
public class Point {

    private float x;
    private float y;


    public Point(float nX, float nY){
        x = nX;
        y = nY;
    }

    public float getY() {
        return y;
    }

    public float getX() {
        return x;
    }

    public boolean inside(float nX, float nY, float radius){
        return (x - radius) < nX && (x + radius) > nX && (y - radius) < nY && (y + radius) > nY;
    }
}
