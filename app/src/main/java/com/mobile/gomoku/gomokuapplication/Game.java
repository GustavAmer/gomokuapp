package com.mobile.gomoku.gomokuapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Gustav on 2015-02-26.
 */
public class Game extends Activity {

    public final static int GRID_SIZE = 15;
    private boolean[][] mUserButtons = new boolean[GRID_SIZE][GRID_SIZE]; //user's button locations by indexes
    private boolean[][] mOpponentButtons = new boolean[GRID_SIZE][GRID_SIZE]; //opponent's buttons' locations by indexes
    private GameBoard mGameBoard;
    View newOfflineGameButton;
    View newOnlineGameButton;
    Communication mCommunication;
    RelativeLayout mConnectionsContainer;
    TextView connId;
    View left;
    View right;
    View select;
    View dialog;
    View okButton;
    EditText nameField;
    private Game thisGame;

    private int listIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_view);

        thisGame = this;

        connId = (TextView) findViewById(R.id.connectionId);
        mConnectionsContainer = (RelativeLayout) findViewById(R.id.connections);
        dialog = findViewById(R.id.name_dialog);
        left = findViewById(R.id.prev);
        right = findViewById(R.id.next);
        select = findViewById(R.id.select);
        okButton = findViewById(R.id.okButton);
        nameField = (EditText) findViewById(R.id.name_field);



        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCommunication.sendName(nameField.getText().toString());
                dialog.setVisibility(View.GONE);
                newOfflineGameButton.setVisibility(View.VISIBLE);
                newOnlineGameButton.setVisibility(View.VISIBLE);
            }
        });

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listIndex--;
                if(listIndex < 0)
                    listIndex = connections.size() - 1;
                connId.setText(connectionIds.get(listIndex));

            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listIndex++;
                if(listIndex == connections.size())
                    listIndex = 0;
                connId.setText(connectionIds.get(listIndex));

            }
        });
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCommunication.sendMessage("start," + connections.get(listIndex));
                mGameBoard.setUserTurn();
                mConnectionsContainer.setVisibility(View.GONE);
            }
        });

        newOfflineGameButton = findViewById(R.id.newOfflineGameButton);
        newOnlineGameButton = findViewById(R.id.newOnlineeGameButton);

        mGameBoard = new GameBoard(getBaseContext());

        emptyArrays();
        LinearLayout game = (LinearLayout) findViewById(R.id.gameBoard);
        game.addView(mGameBoard);

        newOfflineGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    mCommunication.closeSocket();
                }catch (Exception e){}
                mConnectionsContainer.setVisibility(View.GONE);
                mGameBoard.setLocalGame(true);
                emptyArrays();
                mGameBoard.resetGame();
                mGameBoard.setUserTurn();
            }
        });

        newOnlineGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    mCommunication.closeSocket();
                }catch (Exception e){}
                mGameBoard.setLocalGame(false);
                mCommunication = new Communication(thisGame, mGameBoard);
                mGameBoard.setCommunication(mCommunication);
                mGameBoard.resetGame();
                emptyArrays();

                dialog.setVisibility(View.VISIBLE);
                newOfflineGameButton.setVisibility(View.INVISIBLE);
                newOnlineGameButton.setVisibility(View.INVISIBLE);
            }
        });

    }

    private void emptyArrays(){
        for (int i = 0; i < Game.GRID_SIZE; i++){
            for (int j = 0; j < Game.GRID_SIZE; j++) {
                mUserButtons[i][j] = false;
                mOpponentButtons[i][j] = false;
            }
        }
        mGameBoard.setButtonArrays(mUserButtons, mOpponentButtons);
    }

    ArrayList<String> connections;
    ArrayList<String> connectionIds;
    public void displayConnectionsList(ArrayList<String> cs, ArrayList<String> ids) {
        connections = cs;
        connectionIds = ids;
        mConnectionsContainer.setVisibility(View.VISIBLE);
        connId.setText(connectionIds.get(listIndex));

    }
}

