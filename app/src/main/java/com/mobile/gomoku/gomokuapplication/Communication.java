package com.mobile.gomoku.gomokuapplication;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by Gustav on 2015-03-03.
 */
public class Communication {


    private GameBoard gb;
    private Game game;
    public  Communication(Game g,GameBoard gameBoard){
        gb = gameBoard;
        game = g;
        connectWebSocket();
    }
    private WebSocketClient mWebSocketClient;

    public void transferMoves(boolean[][] user, boolean win, int[] winX, int[] winY){
        //send user
        boolean[][] opponent = new boolean[][]{};
        //receive opponent
        sendMessage(prepareMoves(user, win, winX, winY));
    }

    public void closeSocket(){
        try {
            mWebSocketClient.send("close");
            mWebSocketClient.close();
        }catch (Exception e){}
    }

    private void connectWebSocket() {
        URI uri;
        closeSocket();
        try {
            uri = new URI("ws://radiant-ridge-8216.herokuapp.com");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.e("Websocket", "Opened");
                mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
            }

            @Override
            public void onMessage(String s) {
                Log.e("in", s);
                parseMessage(s);
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        try {
            mWebSocketClient.connect();

        }catch (Exception e){
            Log.e("tag", e.getLocalizedMessage());
        }
    }

    public void sendMessage(String s) {
        Log.e("out", s);
        try{
            mWebSocketClient.send(s);
        }catch (Exception e){
            Toast.makeText(game.getBaseContext(), "Problem with connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void sendName(String name){
        sendMessage("name," + name);
    }
    private void parseMessage(String s){
        String[] message = s.split(",");
        if(message[0].equals("moves") || message[0].equals("Win")){
            Log.e("test", "start moves");
            int gridIndexX = 0;
            int gridIndexY = 0;
            boolean [][] moves = new boolean[Game.GRID_SIZE][Game.GRID_SIZE];
            for (int i = 1; i <= Game.GRID_SIZE * Game.GRID_SIZE; i++){
                if(message[i].equals("1")){
                    moves[gridIndexX][gridIndexY] = true;
                }else
                    moves[gridIndexX][gridIndexY] = false;
                gridIndexY++;
                if(gridIndexY == Game.GRID_SIZE){
                    gridIndexX++;
                    gridIndexY = 0;
                }
            }
            int[] winnerX = new int [5];
            int[] winnerY = new int [5];
            int indx = 0;
            if(message[0].equals("Win")){
                for (int i = Game.GRID_SIZE * Game.GRID_SIZE + 1; i <message.length; i+= 2){
                    winnerX[indx] = Integer.parseInt(message[i]);
                    winnerY[indx] = Integer.parseInt(message[i + 1]);
                    indx ++;
                }
            }
            Log.e("test moves out", moves.toString());
            final boolean[][] hMoves = moves;
            final boolean win = message[0].equals("Win");
            final int [] hWinX = winnerX;
            final int[] hWinY = winnerY;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    gb.opponentMoved(hMoves, win, hWinX, hWinY);
                    Log.e("UI thread", "I am the UI thread");
                }
            });

        }else if(message[0].equals("start")){
        }else if(message[0].equals("connections")){
            if(message.length > 1) {
                ArrayList<String> connections = new ArrayList<String>();
                ArrayList<String> ids = new ArrayList<String>();
                for (int i = 1; i < message.length; i++){
                    String[] pieces = message[i].split(";");
                    connections.add(pieces[0]);
                    ids.add(pieces[1]);
                }
                final ArrayList<String> c = connections;
                final ArrayList<String> i = ids;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        game.displayConnectionsList(c, i);
                    }
                });
            }
        }
    }

    private String prepareMoves(boolean[][] moves, boolean win, int[] winX, int[] winY){
        String s;
        if(win)
            s = "Win";
        else
            s = "moves";
        for (int i = 0; i < Game.GRID_SIZE; i++){
            for (int j = 0; j < Game.GRID_SIZE; j++){
                s += ",";
                if(moves[i][j])
                    s +="1";
                else
                    s +="0";
            }
        }
        if(win){
            for (int i = 0; i < 5; i++){
                s +="," + winX[i] + "," + winY[i];
            }
        }
        return s;
    }
}
