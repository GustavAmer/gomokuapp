# README #

### What is this repository for? ###

This repository contains Gomoku application.
Gomoku application is an Android Java application which allows users to play gomoku (aka 5 in row) with each other using a single device or multiple devices with internet connection.


### How do I get set up? ###

Just pull/download the project and import it into Android Studio.


### Who do I talk to? ###

* Repo owner and admin: Gustav Amer - gustavamer@gmail.com

![unnamed.jpg](https://bitbucket.org/repo/B8EGj5/images/1108977385-unnamed.jpg)